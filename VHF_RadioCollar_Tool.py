# -*- coding: utf-8 -*-
"""
Created on Fri Nov 29 13:37:38 2019

@author: vkrat
"""

# This tool appends elevation and land cover data to a set of 
# GPS points taken from VHF radio collars
# Outputs include elevation and land cover appended to point shapefile and 
# a minimum bounding geometry shapefile to represent home ranges of individual animals

import arcpy
arcpy.CheckOutExtension('spatial')

# these are the input VHF radio collar GPS points
points = arcpy.GetParameterAsText(0)

# elevation raster
dem = arcpy.GetParameterAsText(1)

# land cover raster data
landcover = arcpy.GetParameterAsText(2)

# field used to identify individual animals in a study
id_field = arcpy.GetParameterAsText(3)

# minimum bounding geometry to represent home range 
mbg = arcpy.GetParameterAsText(4)

# extract elevation and land cover data from rasters and append to existing point shapefile 
arcpy.sa.ExtractMultiValuesToPoints(points, [(dem, 'Elevation'), (landcover, 'Land Cover')])

# create a minimum bounding geometry using the points from the shapefile. Create separate polygons for each individual
# animal represented in the dataset using id_field
arcpy.MinimumBoundingGeometry_management(points, mbg, 'convex_hull', 'list', id_field)

# add a field type "float" to the mbg polygon shapefile to calculate acres
arcpy.AddField_management(mbg, field_name="area_acres", field_type="FLOAT")

# calculate acreage of each minimum bounding geometry polygon
arcpy.CalculateField_management(mbg, "area_acres", '!shape.area@acres!',"PYTHON")

# write message "done" when tool is done runnning
arcpy.AddMessage('Done!')